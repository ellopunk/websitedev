---
title: "Podcasts"
#description: ""
#summary: ""
date: 2024-02-12
lastmod: 2024-02-12
draft: false
menu:
  docs:
    parent: ""
    identifier: "Past-proj-podcast"
weight: 20
toc: true
---
## Guest Appearances
- **Screaming in the Cloud**: [Security Can Be More than Hues of Blue with Ell Marquez](https://share.transistor.fm/s/11e8c579)
- **Career Notes**: [Ell Marquez: It's Okay To Be New](https://youtu.be/EBNpLbBSbPc)
- **CyberWire-X**: [APTs Transitioning to the Cloud](https://thecyberwire.com/podcasts/cyberwire-x/15/notes)
- **Cloud Unfiltered**: [Ep97: How to Keep Your Skill Set Fresh - Ell Marquez & Christophe](https://podcasts.apple.com/us/podcast/ep97-how-to-keep-your-skill-set-fresh-ell-marquez-christophe/id1215105578?i=1000464286143)
- **Linux Spotlight**: [EP07](https://www.youtube.com/watch?v=FuVX5yJvl1M)
- **User Error**: [Stranger Distro Danger](https://fireside.fm/s/WUDzse_C+ZYlw3wfp)
- **Linux Unplugged**: [Ell is for Linux](https://fireside.fm/s/RUkczH-V+cdOLScyT) and reoccurring guest as of Feb 2019.
- **Linux Unplugged**: [Were gonna need a bigger repo](https://www.jupiterbroadcasting.com/129361/were-gonna-need-a-bigger-repo-linux-unplugged-288/)
- **Late Night Linux**: [Episode 56](https://latenightlinux.com/late-night-linux-episode-56/)
- **Jupiter Broadcasting's YAML Study Group**: [YAML Essentials](https://github.com/JupiterBroadcasting/CommunityNotes/blob/master/yaml_essentials/yaml_notes.md)
- **The Friday Stream**: [Mic and Coke](https://www.jupiterbroadcasting.com/131906/mic-and-coke-the-friday-stream-6/)
- **Jupiter Broadcasting's Linux Operating System Fundamentals Study Group**: [Meetup](https://www.meetup.com/jupiterbroadcasting/events/258602577/)

- **CyberSpeaks Live**:
  - [Offensive Security OSCP Exam Review](https://anchor.fm/cyberspeakslive/episodes/Offensive-Security-OSCP-Exam-Review-ei5ipe)
  - [Mental Health in InfoSec with Alethe Denis](https://anchor.fm/cyberspeakslive/episodes/Mental-Health-in-InfoSec-with-Alethe-Denis-ehni9k)

## Choose Linux
- [12: Regolith, Rosa, and Antsy Alien Attack](https://chooselinux.show/12) Two new hosts join Joe to talk about a nice i3 implementation, and an amazing arcade game written in Bash.
- [13: Qubes OS + Plex vs Kodi](https://chooselinux.show/13) Distrohoppers brings us a fascinating distro where every application runs in its own VM. Plus Drew and Joe disagree on the best media solution.
- [14: Endeavour OS + Pisi Linux](https://chooselinux.show/14) We take a look at the continuation of Antergos called Endeavour OS and are pretty impressed, and Distrohoppers delivers an interesting distro that's obsessed with cats.
- [15: OBS Studio + Endless OS](https://chooselinux.show/15) Distrohoppers delivers a distro that divides us, and we check out the streaming and recording software OBS Studio.
- [16: PCLinuxOS + Hugo](https://chooselinux.show/16) We check out a great tool for learning web development basics, and Distrohoppers brings us mixed experiences.
- [17: Hardware hacking basics, Slackel + OSCAR](https://chooselinux.show/17) Getting into hardware hacking with Arduino, and analysing sleep data from CPAP machines.
- [18: Introducing New People to Linux](https://chooselinux.show/18) There's lots to consider when setting someone up with Linux for the first time. User needs and expectations, distro choice, hardware, and so much more. We discuss our experiences, and ask some fundamental questions.
- [19: Android-x86 + First Steps into the Cloud](https://chooselinux.show/19) We have three different approaches to using the cloud, so we discuss various ways to expand your Linux knowledge beyond the desktop. Plus Distrohoppers delivers a mobile-like experience that splits opinion.
- [20: Single Board Computers](https://chooselinux.show/20) We are joined by special guest Chz who is a long-time user of single board computers to talk about how we use boards like the Raspberry Pi, Orange Pi, and ROCKPro64.
- [21: KaOS + How We Install Software](https://chooselinux.show/21) There are numerous ways to install software on a modern Linux system and we each have a different approach.
- [22: Finding Your Community](https://chooselinux.show/22) We talk about the best ways to get involved in open source communities, finding like-minded people, conference strategies, community hubs, and what happened to all the LUGs.
- [23: Void Linux + Contributing to Open Source](https://chooselinux.show/23) A chance to learn some Linux fundamentals in Distrohoppers, and the numerous ways we can all contribute to Linux and open source.
- [24: What We Wish We'd Known Earlier](https://chooselinux.show/24) All three of us have different levels of experience with Linux but there are tons of things that we wish we'd learned earlier in our journey.
- [25: Tails + Virtualization](https://chooselinux.show/25) Ultimate privacy in Distrohoppers, and the best ways to run other operating systems within your current Linux distro.
- [26: Explaining Linux and Open Source as Concepts](https://chooselinux.show/26) Trying to explain what Linux and open source are can be tricky. We discuss our various approaches, and how they differ depending on the experience of who we are explaining them to.
- [27: GhostBSD + Freedom vs Pragmatism](https://chooselinux.show/27) Distrohoppers serves up something very different in the form of desktop BSD, and we reveal how important freedom is to us all.
- [28: What We Love About Linux](https://chooselinux.show/28) Valentine's Day is nearly here so it's time to talk about why we love Linux and open source. Nothing is perfect though, so we also touch on a few areas that we feel could be improved.
- [29: Linux Console + Boutique Distros](https://chooselinux.show/30) A confusing experience in Distrohoppers which raises deeper questions about the value and viability of smaller distros.
- [30: Project Catch Up](https://chooselinux.show/30) We revisit some of the projects we have covered in previous episodes to see what we've stuck with and what we haven't.
- [31: Solus + Visual Studio Code](https://chooselinux.show/31) We try out Solus and are all impressed by this independent distro. Then Ell and Drew sing the praises of Visual Studio Code - a text editor that's packed full of features.
- [32: Windows as a Linux User + Sway Window Manager](https://chooselinux.show/32) Ell tells us about her first ever experience with Windows 10 and how it compares with Linux. Plus Drew has been using a Wayland-based i3-like tiling window manager called Sway.


## Jupiter Extras

- [Building an Open Source Community: Wirefall](https://www.listennotes.com/podcasts/jupiter-extras/building-an-open-source-m3lV40cgyK9/)
  Ell and Wes sit down with Wirefall, founder of the Dallas Hackers Association, to talk about the struggles and rewards of community building, why moving with the times is key, and how to foster an inclusive community meetup that still feels like a family gathering.

- [Mastering The Basics](https://www.listennotes.com/podcasts/jupiter-extras/mastering-cyber-security-H8wbe4X-uNE/)
  Wes and Ell sit down with James Smith to have an honest conversation about what skills are needed to start a career and be successful in Tech and Information Security.

- [Pentesting Problems: Bryson Bort](https://www.listennotes.com/podcasts/jupiter-extras/pentesting-problems-bryson-nvlM90jWNdC/)
  Ell sits down with Bryson Bort to discuss pentesting with Scythe, Red Team vs Blue Team operations, and the benefits that a Purple Team might have on the industry.

- [Learning, Failing, and Hacking the Industry: Danny Akacki](https://www.listennotes.com/podcasts/jupiter-extras/learning-failing-and-hacking-BuAYyfJWZgp/)
  Ell sits down with Danny Akacki to talk about infosec, his experience on the Blue Team, how PancakesCon got started, and more.

- [Kubernetes 411](https://www.listennotes.com/podcasts/jupiter-extras/infrastructure-engineer-seth-Kom0sG8A4Xy/)

- [AWS 411](https://www.listennotes.com/podcasts/jupiter-extras/makerspace-101-brian-beck-3kNH6FrcZji/)

- [Cyber Security Mistakes You're Probably Making: Duncan McAlynn](https://www.listennotes.com/podcasts/jupiter-extras/cyber-security-mistakes-FNBslrLSHdP/)

- [Operation Safe Escape](https://www.listennotes.com/podcasts/jupiter-extras/operation-safe-escape-MXiNKuILaoE/)

- [Threat Hunting 101](https://www.listennotes.com/podcasts/jupiter-extras/building-an-open-source-m3lV40cgyK9/)

- [Interview with Security Analyst Lou Stella](https://www.listennotes.com/podcasts/jupiter-extras/interview-with-security-s6XetGMhyU0/)

- [Mental Health Hackers](https://www.listennotes.com/podcasts/jupiter-extras/mental-health-hackers-AmR4fv0VS6g/)

- [411 DevSecOps: Karthik Gaekwad](https://www.listennotes.com/podcasts/jupiter-extras/kubernetes-411-hart-hoover-fqWew_9opvG/)

- [Infrastructure Engineer: Seth McCombs](https://www.listennotes.com/podcasts/jupiter-extras/infrastructure-engineer-seth-Kom0sG8A4Xy/)

- [Makerspace 101: Brian Beck](https://www.listennotes.com/podcasts/jupiter-extras/makerspace-101-brian-beck-3kNH6FrcZji/)

- [Ell's Trip to Hacker Summer Camp](https://www.listennotes.com/podcasts/jupiter-extras/ells-trip-to-hacker-summer-ukGAz97nx3j/)

- [Brunch with Brent: Ell Marquez](https://www.listennotes.com/podcasts/jupiter-extras/brunch-with-brent-ell-marquez-wzPELMAFH1q/)


