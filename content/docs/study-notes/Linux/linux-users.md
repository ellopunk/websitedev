---
Title: "Managing Linux Users Cheat Sheet"
lastmod: 2024-09-17
draft: false
menu:
  docs:
    parent: "NLC"
weight: 401
---

## Adding and Removing Users


| Command | Description |
|---------|-------------|
| `useradd <username>` | This command adds a new user to the system. Replace `<username>` with the actual username you want to create. |
| `useradd -m <username>` | Adds a new user **and** creates a home directory for them at `/home/<username>`. Without the `-m` flag, no home directory will be created. |
| `useradd -G <groupname> <username>` | Adds a user to a specific **additional** group. By default, a user is added to their own group, but you can assign them to other groups using this command. |
| `useradd -s <shell> <username>` | Specifies which shell the user will use. The default is usually `/bin/bash`, but you can set others like `/bin/sh`. |
| `userdel <username>` | Deletes a user from the system, but does **not** remove their home directory. |
| `userdel -r <username>` | Deletes the user **and** their home directory, along with any files they own in `/home/<username>`. Use this with caution. |



## Modifying Users

| Command | Description |
|---------|-------------|
| `usermod -l <newusername> <oldusername>` | Changes the login name of an existing user. Useful when renaming a user. |
| `usermod -aG <groupname> <username>` | Adds a user to an additional group, **without** removing them from their current groups. The `-a` stands for "append". |
| `usermod -L <username>` | Locks a user’s account, preventing them from logging in. The user’s files remain intact. |
| `usermod -U <username>` | Unlocks a previously locked user account. |


## Adding Groups

- Groups are collections of users. They help simplify permissions management. For example, adding multiple users to the "developers" group makes it easier to assign the same access to all developers.

| Command | Description |
|---------|-------------|
| `groupadd <groupname>` | Creates a new group. Replace `<groupname>` with the name of the group you want to create. |
| `groupdel <groupname>` | Deletes a group. Be cautious—deleting a group may affect users associated with it. |



## Modifying Groups

- Just like users, groups can be renamed using `groupmod`. 

| Command | Description |
|---------|-------------|
| `groupmod -n <newgroupname> <oldgroupname>` | Changes the name of an existing group. |


## Checking Users and Groups

- The `/etc/passwd` file contains essential information about user accounts, such as their username, user ID, and home directory.
- The `/etc/shadow` file stores encrypted password data and expiration dates.
- The `/etc/group` file lists all the system groups and the users assigned to them.

| Command | Description |
|---------|-------------|
| `cat /etc/passwd` | Displays a list of all the user accounts on the system. |
| `cat /etc/shadow` | Shows password and account expiration information. This file is only viewable by the root user. |
| `cat /etc/group` | Displays a list of all the groups on the system and their members. |

## Password Management

| Command | Description |
|---------|-------------|
| `passwd <username>` | Changes the password for a specific user. |
| `passwd -l <username>` | Locks a user’s password, preventing them from logging in. |
| `passwd -u <username>` | Unlocks a locked password, allowing the user to log in again. |



