---
Title: "One Liner Examples"
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: "Linux"
weight: 300

---

## Customer Ticket 1

You can use the following one-liner to create 10 files named `Report1`, `Report2`, ..., `Report10` in your home directory:

```bash
touch ~/Report{1..10} && date && ls ~/Report*

```

### `&&` or `;` 

`&&` **(Logical AND)**
- `&&` is used to chain commands, but the second command only runs if the first command is successful. It is typically used when you want to run the next command only if the previous command succeeds.

`;` **(simicolon)**
- The semicolon `;` runs the commands in sequence, regardless of whether the previous command succeeds or fails.

## Customer Ticket 2

```bash
mkdir /home/student/marketing && mv /home/student/Report* /home/student/marketing/ && touch /home/student/marketing/ReadMe&& echo "Reports for each year will be organized into directories named for each corresponding year - ell" > /home/student/marketing/ReadMe
```
**But can I make it shorter?**

You can simplify this one-liner by using tilde ~ to represent /home/student and combining the commands in a more concise way:

```bash
mkdir ~/marketing && mv ~/Report* ~/marketing/ && echo "Reports for each year will be organized into directories named for each corresponding year - ell" > ~/marketing/ReadMe && cat ~/marketing/ReadMe
```

### Customer Ticket 3

```bash
rm ~/Report{2..10} && mv ~/Report1 ~/SeptemberReport && ls -l ~/
```
**Creating a symlink**
```bash
ln -s /path/to/original/file /path/to/symlink
```

## Redhat Chapter 3
 
 **Task:** Create a Directory `Thesis`, subdirectories `Chapter 1 - Chapter5` and inside of `Thesis/Chapter4` create a file called `lesson1`.
 
 ```bash
 mkdir -p Thesis/Chapter{1..5} && touch Thesis/Chapter3/lesson1 && ls -R Thesis
 ```