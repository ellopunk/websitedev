---
Title: "File Globbing Cheat Sheet"
lastmod: 2024-09-17
draft: false
menu:
  docs:
    parent: "Linux"
weight: 201
---

# Linux File Globbing Cheat Sheet

| Glob          | Description                                                  |
|---------------|--------------------------------------------------------------|
| `*`           | Match zero or more characters in a filename                  |
| `?`           | Match exactly one character                                  |
| `[abc...]`    | Match any one of the enclosed characters                     |
| `[!abc...]`   | Match any character not enclosed                             |
| `[a-z]`       | Match any character in the specified range (lowercase)       |
| `[A-Z]`       | Match any character in the specified range (uppercase)       |
| `[0-9]`       | Match any digit                                              |
| `[[:alpha:]]` | Match any alphabetic character                               |
| `[[:digit:]]` | Match any numeric character                                  |
| `[[:alnum:]]` | Match any alphabetic or numeric character                    |
| `[[:space:]]` | Match any whitespace character                               |
| `[[:punct:]]` | Match any punctuation character                              |
| `[^abc...]`   | Match any character except those in the set                  |
| `\`           | Escape character to match special glob characters literally  |
