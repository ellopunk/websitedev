---
title: "OS Study Notes"
description: "Cheat sheets and quick os info guides."
summary: ""
draft: false
menu:
  docs:
    parent: "study-notes"
    identifier: "qick guides"
weight: 600
---