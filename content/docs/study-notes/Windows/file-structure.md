---
Title: "Windows File Structure Overview"
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: "Windows"
weight: 1
---
## Drives
**Root Directory (C:):** Windows is typically installed on the C: drive. However, additional drives (D:, E:, etc.) can create other partitions, external drives, or network drives.

## Folders
Each drive has its own file system and directory structure, with the root directory at top.

**System Folders:** Contain critical system files
- **C:\Windows:** Contains the Windows operating system files.
- **C:\Program Files and C:\Program Files (x86):** Folders where applications are typically installed. The (x86) version is for 32-bit applications on 64-bit systems.
- **C:\Users:** Stores user profiles and associated data (documents, settings, etc.) for each user account on the machine.

## Paths Structure
`Drives/Directories(folders)/Subdirectories(SubFolders)/Files.extenstion`

**Drives:** The highest level in the hierarchy.
`C:\`

**Directories (Folders):** Also called directories, these are containers where files and other directories (called 
**subdirectories**) are stored.
- Directory: `C:\Usrs\Student\` 
-Sub-directory: `C:\Usrs\Student\Documents`

**Files:** Contents stored in the directories or subdirectories. Each file has a name and an extension (e.g., .txt, .docx), which identifies the file type.
`C:\Users\Student\Documents\classnotes.txt`
