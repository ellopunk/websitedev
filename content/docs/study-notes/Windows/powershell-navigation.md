---
Title: "PowerShell Navigation and File Manipulation"
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: "Windows"
weight: 40
---


## Navigation

### pwd

Present working directory, tells you where in the file structure path you are. 

```powershell
pwd
```
**Sample Output:** 

```
PS C:\Users\ellma\> pwd

Path
----
C:\Users\ellma\
```
### cd

Allows you to move between directories.

Starting at `C:\Users\<enter your user name>\` type: 

```powershell
cd Documents
```
Now run: 

```powershell
pwd
```
New Output:
```
Path
----
C:\Users\ellma\Documents
```

### New-Item
Used to create new items.  files, folders, registry keys, or other objects in a specified location. What types of items can you create?
- File – to create a file.
- Directory – to create a folder (or directory).
- SymbolicLink – to create a symbolic link (shortcut).
- HardLink – to create a hard link.
- Junction – to create a directory junction (similar to a symbolic link but for directories).

```
New-Item -Path <path> -Name <name> -ItemType <type>
```
Let's start off with a simple task, creating a new file: 

```powershell
New-Item -Path C:\Users\ellma\Documents
```

Output:
```
    Directory: C:\Users\ellma\Documents

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----         9/22/2024   3:17 PM              0 firstfile.txt
```

#### Creating Directories
What if you wanted to create a new directory? You have options! 

Use New-Item
```powershell
New-Item Subdir -ItemType Directory
```
The `md`can be used but the command syntax changes.
```powershell
md "C:\Users\ellma\Documents\subdir2"
```
The Linux command `mkdir` command is also an option.
```powershell
mkdir "C:\Users\ellma\Documents\subdir3
```

### Listing Items in Directories
While Powershell has the command `Get-ChildItem,` the `ls` command is set as an alias, and because Powershell is "fun," `dir` is also an alias for `Get-ChildItem.`

You could run:

```powershell
Get-Child -Path C:\Users\ellma\Documents
```
or

```powershell
ls "C:\Users\ellma\Documents"
```
or
```powershell
dir -Path "C:\Users\YourUsername\Documents" -Filter *.txt
```
The output would remain the same:

Output:
```
    Directory: C:\Users\ellma\Documents


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         9/22/2024   3:26 PM                Subdir
-a----         9/22/2024   3:29 PM              0 firstfile.txt
```

### Copying Items

There are several options to copy files in PowerShell
```
copy sourceFile.txt destinationFolder
```
Example:
``` powershell
cp "C:\Users\ellma\Documents\exampledoc" "C:\Users\ellma\Documents\secondfile"
```
The Linux Command cp is available but does not offer all the features you might need, especially when handling many files or directories.
```
cp sourceFile.txt destinationFolder
```
**Alternative Commands:**
- **xcopy:** A more powerful command for copying directories, subdirectories, and files, including hidden ones. It is more feature-rich than copy.

```powershell
xcopy sourceDir destinationDir /s /e
```
- `/s` copies directories and subdirectories, except empty ones.
- `/e` copies directories and subdirectories, including empty ones.

- **robocopy:** Recommended for copying large numbers of files or directories.

```powershell
robocopy sourceDir destinationDir /mir
```
### Removing Items
In creating this guide, I've made more example directories and files than I need. Time to remove a few. 

Current status: 
```
PS C:\Users\ellma\Documents> ls


    Directory: C:\Users\ellma\Documents


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         9/22/2024   3:26 PM                Subdir
d-----         9/22/2024   4:00 PM                subdir2
d-----         9/22/2024   4:00 PM                subdir3
-a----         9/22/2024   3:17 PM              0 exampledoc
-a----         9/22/2024   3:29 PM              0 secondfile.txt
```
**Removing a File**

`Remove-Item -Path "C:\path\to\file.txt"`

```powershell
Remove-Item -Path "C:\Users\ellma\Documents\exampledoc"
```

**Removing a Directory**
- Remove an empty directory
`Remove-Item -Path "C:\path\to\directory"`

```powershell
Remove-Item -Path "C:\Users\ellma\Documents\subdir3"
```
I also created files inside of subdir2

- Remove a directory and its contents
``Remove-Item -Path "C:\path\to\directory" -Recurse``

