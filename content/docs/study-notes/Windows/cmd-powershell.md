---
Title: "CMD -vs- Powershell"
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: "Windows"
weight: 20
---
## CMD VS PowerShell

**CMD (Command Prompt):** CMD is a legacy Command-Line Interpreter, not a Command-Line Interface. It was designed for simple commands and operations, such as file manipulation and bash scripting. Because CMD processes data as plain tests, it is difficult to perform complex data manipulation. 

- **CLI (Command Line Interface):** The environment where you enter and execute PowerShell commands. 

- **Command Line Interpreter:** The software that processes and interprets the commands you enter.

- **File Manipulation** includes but is not limited to tasks like creating, reading, and modifying files. 

**PowerShell**: A Command-Line shell is designed to automate administrative tasks and manage configurations. It can execute more complex commands and scripts that work with objects, not just text.

- **Shell:** The shell interprets your commands, manages the environment, and executes those commands.