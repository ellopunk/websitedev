---
Title: "PowerShell Tools"
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: "Windows"
weight: 50
---
## CLI Tools
{{< callout context="note" title="Note" icon="info-circle" >}} 
Several commands that are helpful tools in PowerShell can be used on other operating systems.
{{< /callout >}}

### net user

```powershell
net user
```
Sample Output:
```
User accounts for \\COMPUTER-NAME
-------------------------------------------------------------------------------
Administrator  Guest  Student
```
- Create a new user
Try: 
```powershell
net user newuser /add
```
- Delete User You Created
```powershell
net user newuser /delete
```

### net use
The net use command is used to connect to, manage, or disconnect from shared network resources (such as shared drives or printers). It allows you to map network drives, view active connections, or remove them.

```bash
net use
```
Sample Output:
```
New connections will not be remembered.

Status       Local     Remote                    Network
-------------------------------------------------------------------------------
OK           X:        \\Server\SharedFolder     Microsoft Windows Network
OK           Y:        \\Server\Printer          Microsoft Windows Network

```
### hostname
Displays the name of the host system you are on
```powershell
hostname
```
Sample Output:

```
BestHostNameEver
```

***Fun Fact:***
On Linux `hostname -i` - Displays the IP address of the system (for systems with a configured DNS).

However when run on PowerShell you will get the following error:
```
PS C:\Users\ellma\Documents> hostname -i
sethostname: Use the Network Control Panel Applet to set hostname.
hostname -s is not supported.
```
If you want to find your IP address in Windows PowerShell you need to use `ipconfig` or `Get-NetIPAddress`.

{{< callout context="note" title="Note" icon="info-circle" >}} 
For further information on the commands below, talk to your networking person! Don't bully me. 
{{< /callout >}}
### ipconfig
The command `ipconfig` run alone displays your IP address.
```powershell
ipconfig
```
or 
```powershell
Get-NetIPAddress
```

Example Output: 
```
Windows IP Configuration

Ethernet adapter Ethernet:

   Connection-specific DNS Suffix  . : yourdomain.com
   Link-local IPv6 Address . . . . . : fe80::d4a5:2cfa:e3b5:cf2d%12
   IPv4 Address. . . . . . . . . . . : 192.168.1.5
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 192.168.1.1

Wireless LAN adapter Wi-Fi:

   Connection-specific DNS Suffix  . : 
   Link-local IPv6 Address . . . . . : fe80::d4a5:2cfa:e3b5:cf2d%15
   IPv4 Address. . . . . . . . . . . : 192.168.1.10
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 192.168.1.1

Tunnel adapter Teredo Tunneling Pseudo-Interface:

   Connection-specific DNS Suffix  . : 
   IPv6 Address. . . . . . . . . . . : 2001:0:3487:1200:38b7:e0aa:9f9b:f288
   Link-local IPv6 Address . . . . . : fe80::38b7:e0aa:9f9b:f288%3
   Default Gateway . . . . . . . . . : 

Tunnel adapter isatap.{D94BB44D-CA8C-4DF7-82C9-C52F897B56AC}:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . : 
```
| Option                  | Description                                                                 |
|-------------------------|-----------------------------------------------------------------------------|
| `ipconfig /all`          | Displays full detailed network configuration for all interfaces.            |
| `ipconfig /release`      | Releases the current DHCP-assigned IP addresses for all or specific adapters.|
| `ipconfig /renew`        | Renews DHCP-assigned IP addresses for all or specific adapters.             |
| `ipconfig /flushdns`     | Clears the DNS resolver cache.                                              |
| `ipconfig /registerdns`  | Refreshes and registers DNS names with the DNS server.                     |
| `ipconfig /displaydns`   | Shows the current DNS resolver cache.                                       |
| `ipconfig /allcompartments` | Displays all compartments for network interfaces.                       |

### ping 
Test connectivity between your computer and a remote system. Ping can also measure latency, the time it takes for packets to travel to the destination and back in milliseconds.

Ping can help you troubleshoot. If the ping command fails, it can indicate an issue with network configuration or routing. (It may also be the target system is down or blocking ICMP traffic, but that's a conversation for another time.)

- Syntax
`ping [hostname or IP address]`

Example: 
```powershell
ping 8.8.8.8
```

Sample Output: 
```
Pinging 8.8.8.8 with 32 bytes of data:
Reply from 8.8.8.8: bytes=32 time=37ms TTL=118
Request timed out.
Reply from 8.8.8.8: bytes=32 time=37ms TTL=118
Reply from 8.8.8.8: bytes=32 time=46ms TTL=118

Ping statistics for 8.8.8.8:
    Packets: Sent = 4, Received = 3, Lost = 1 (25% loss),
Approximate round trip times in milli-seconds:
    Minimum = 37ms, Maximum = 46ms, Average = 40ms
```
`Pinging 8.8.8.8 with 32 bytes of data:` I pinged 8.8.8.8 (Google's public DNS server and a 32 bytes of data indicates the size of the packet being sent in each ping request.)

Reply from the server:

```Reply from 8.8.8.8: bytes=32 time=37ms TTL=118
Request timed out.
Reply from 8.8.8.8: bytes=32 time=37ms TTL=118
Reply from 8.8.8.8: bytes=32 time=46ms TTL=118
```
- **bytes=32** The size of the data packet sent and received is 32 bytes.

- **time=37ms** The round-trip time (how long it took the packet to travel to 8.8.8.8 and back) is 37 milliseconds.

- **TTL=118** Time to Live (TTL) indicates how many "hops" the packet can pass through (routers) before it is discarded. (Each router decreases this value by 1.)

- **Request timed out** This means one of the packets did not receive a reply. Possible causes: Temporary network congestion, packet loss or the server or a router along the path might have temporarily dropped the packet.


### netstat
netstat (network statistics) displays network connections, routing tables, interface stat
```powershell
netstat
```
Common Options in Windows:
| Option      | Description                                                                 |
|-------------|-----------------------------------------------------------------------------|
| `-a`        | Displays **all** connections and listening ports.                           |
| `-n`        | Displays addresses and port numbers in **numerical form** (i.e., no DNS lookup). |
| `-o`        | Displays the **process ID (PID)** associated with each connection.           |
| `-b`        | Shows the **executable** involved in creating each connection (requires admin privileges). |
| `-r`        | Displays the **routing table**.                                              |
| `-e`        | Displays **Ethernet statistics** (packets sent/received, errors).            |
| `-p [proto]`| Shows connections for a specific **protocol** (e.g., TCP, UDP).              |
| `-s`        | Displays statistics for each **protocol** (e.g., TCP, UDP, ICMP).            |
| `-f`        | Displays the **fully qualified domain name (FQDN)** for foreign addresses.   |

***Fun Fact***: On modern Linux distros, the netstat tool is part of the net-tools package. Although this package is deprecated in favor of ss (socket statistics), people are set in their ways and still use netstat.

### nslookup
nslookup (nameserver lookup)
`nslookup [hostname] [server]`
Try:
```powershell
nslookup google.com
```
Sample Output: 
```
server:  dns.googleAddress:  8.8.8.8Non-authoritative answer:Name:    google.comAddress: 142.250.190.14
```
### tracecert
Network diagnostic tool used to trace the path that packets take from your computer to a destination (such as a domain or IP address) and to measure the delay between hops along the way.
`tracert [hostname or IP address]`
Try
```powershell
tracert google.com
```
Sample Output: 
```
Tracing route to google.com [142.250.190.14]
over a maximum of 30 hops:

  1     1 ms     1 ms     1 ms  192.168.1.1
  2    10 ms     8 ms    11 ms  10.0.0.1
  3    15 ms    14 ms    15 ms  198.51.100.1
  4    22 ms    20 ms    21 ms  203.0.113.5
  5    30 ms    28 ms    27 ms  142.250.190.14

Trace complete.
```

- **chkdisk**


- **format**
`