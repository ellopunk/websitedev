---
Title: "Configuring Git Integration in VS Code Using PowerShell"
lastmod: 2024-02-12
draft: false
menu:
  docs:
    parent: "Git"
weight: 10
toc: true
---

If Visual Studio Code is not installed on your system, instructions on installation  can be found at [code.visualstudio.com](https://code.visualstudio.com). 

## Open VS Code and Integrated Terminal

Use the shortcut `Ctrl+` to open the integrated terminal.

## Ensure Git is Installed on Your System

To check if Git is installed, open a terminal and run:

```bash
git --version
```

If it is not installed, instructions on installation can be found at [git-scm.com](https://git-scm.com).

## Setup Environmental Variables

By default, Git is usually installed in one of these locations:
- **C:\Program Files\Git\bin\git.exe**
- **C:\Program Files (x86)\Git\bin\git.exe**

### Add Git To the System Path

**For Windows 10 and Windows 11:**

1. Search for Environment Variables:
    - Right-click the Start button and select `System`.
    - Click on `About`, then select `System info` at the bottom right.
    - On the left sidebar, click on `Advanced system settings`.
    - A System Properties window will pop up. Here, click on the `Environment Variables` button near the bottom.

2. Edit System PATH:
    - In the Environment Variables window, under the `System variables` section, find and select the `Path` variable, then click `Edit...`.
    - If the path to Git is not there, add it:
        - Click `New` and enter the path to your Git bin directory. This is usually installed in one of these locations:
            -  **C:\Program Files\Git\bin** 
            - **C:\Program Files (x86)\Git\bin**
    
3. Restart Your System (Optional but Recommended)

## Verify the Changes
 ```bash
 git --version
 ```

## Configure Git

To configure Git, run the following commands:

*Note: Enter your name and email in the corresponding " " sections*

```bash
git config --global user.name "Your Name"
git config --global user.email "your.email@example.com"
```
