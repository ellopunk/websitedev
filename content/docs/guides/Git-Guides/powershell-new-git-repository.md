---
Title: "Creating a New Git Repository using PowerShell"
lastmod: 2024-02-12
draft: false
menu:
  docs:
    parent: "Git"
weight: 20
toc: true
---


> For this example, I will use the directory `git` and the `subdirectory` 'portfolio', which will become my new repository on GitLab.

## Creating Directory and Subdirectory

```bash
New-Item -Path '.\Git\Portfolio' -ItemType Directory
```
{{< callout context="note" title="Note" icon="info-circle" >}} 
Some individuals use the flag-force to ensure that both the Git and Portfolio directories are created if they don't already exist; however, this is a step I've never required and thus will not include it here. 
{{< /callout >}}


List directories
```bash
ls
```
Will show the following output: 

```
Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         2/22/2024  10:18 AM                Portfolio

```

## Convert Portfolio into a Repository

1) Change directories
```bash
cd Portfolio
```

2) Initiate project

```bash
git init
```
A .git folder is created in the directory that contains Git records and configuration files. To ensure that your repository works as intended to not edit these files. 

```
Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d--h--         2/22/2024  10:57 AM                .git
```

## Add a remote

Adding a remote will tell Git that your new remote repository is tied to the portfolio folder. 

```bash
git remote add origin git@gitlab.com:ellopunk/portfolio.git
```

## Create README file

Though you can create any file or any amount of files to initiate your repository, the best practice is to create a README.md file. The README.MD file should provide essential information that helps others understand the project in it, how to use it, and how to contribute to the project if you accept contributions. 

 ```bash
 New-Item README.md
```

## Prepare for First Commit

1) add README.md file. 

```bash
 git add README.md
 ```

`git add` moves changes from the working directory to the staging area and tells Git which modifications (added, modified, deleted files) you want to include in your next commit.

2) Commit Files

```bash
 git commit -S -m "Initializing Project"
```
`git commit` takes the added files and creates a new commit representing the changes in the repository's history. 

`-m` Every commit requires a commit message; write meaningful commit messages that communicate the intent of the changes.

`-S` will prompt you to sign your commit. For additional information see [Setting up GPG Keys with Powershell](docs/guides/git-guides/signing-git-commits-using-powershell/)

3) Push Files

```bash
git push --set-upstream origin master
```

`git push` uploads local repository content to a remote repository. 

`--set-upstream origin master` Sets the default upstream (remote) branch for the current local branch. 

`origin:` This specifies the remote repository where you're pushing your changes. 

`master:` This specifies the branch you're pushing to on the remote repository. 

{{< callout context="note" title="Note" icon="info-circle" >}} 
 Main or other names are now being used as the default branch to avoid the master-slave terminology 
{{< /callout >}}

4) Celebrate

Congratulations! Your project is ready; you can confirm visually by visiting your GitLab page. 