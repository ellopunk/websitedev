---
Title: "Signing Git Commits Using PowerShell"
lastmod: 2024-02-12
draft: false
menu:
  docs:
    parent: "Git"
weight: 30
toc: true
---

***Guide written for Windows Version: 11 Pro***

If you do not have GPG Keys setup refer to [Setting up GPG Keys with Powershell](/docs/guides/setting-up-gpg-keys-with-powershell/)



## Configuring Git Global Variables

1. List gpg keys

*Note: The key ID is the part after / on the line starting with "sec"*

 ```bash
 gpg --list-secret-keys --keyid-format LONG
 ```

2. Configure Git to Use Your GPG key

```bash 
git config --global user.signingkey YOUR_KEY_ID
```

3. *Optional:* To configure Git to sign all your commits by default

```bash 
git config --global commit.gpgsign true
```
4.  Print your public GPG Key
```bash 
gpg --armor --export YOUR_EMAIL_ADDRESS
```

## Adding Your GPG key to GitLab

- Sign in to GitLab
- Go to your user settings, then to the "GPG Keys" section.
- Paste your public GPG key into the text area and click "Add key."

5. Verify 

- Commit and push changes to your repository. 

```bash 
git commit -S -m "Your commit message"
```


### Common Errors

#### 1. No Secret Key

```
gpg: skipped "A6D52EAF2B1C52E5": No secret key
gpg: signing failed: No secret key
error: gpg failed to sign the data
fatal: failed to write commit object
```

**Background Information:**

- Git for Windows comes with a minimal version of GnuPG. This version uses the `~/.gnupg/`, `C:\Users\YourUsername\.gnupg\` ,  directory for configuration files and key storage. 


- gpg4win configures GnuPG to use` %APPDATA%\gnupg` for configuration files and key storage.


1. Check Git global configuration

```bash
git config --global --list
```

Look for `gpg.program` variable.  If there is no `gpg.program` entry, Git will attempt to use the minimal install discussed above.

```js {hl_lines=5}
user.name=Your name
user.email=ell@myemail.com
user.signingkey= 12334asd3
commit.gpgsign=true
```
2. Set the GPG Program in Git Configuration 

```bash
git config --global gpg.program "C:/Program Files (x86)/GnuPG/bin/gpg.exe"
```
{{< callout context="attention" title="Attention" icon="info-circle" >}} 
Your path may differ; please confirm before running this command.
{{< /callout >}}

3. Confirm Change

```bash
git config --global --list
```

```js {hl_lines=5}
user.name= Your Name
user.email=you@youremail.com
user.signingkey= 12334asd3
commit.gpgsign=true
gpg.program=C:/Program Files (x86)/GnuPG/bin/gpg.exe
```

#### 2. Bad Data Signature

```
gpg: bad data signature from key PROBLEMID: Wrong key usage (0x19, 0x2)
```

1) Check for Matching Keys

- List the key ID Git is configured to use 

```bash
git config --global user.signingkey
```
- Check for the Matching Key

```bash
gpg --list-secret-keys --keyid-format LONG
```

#### 3. No Agent Running

```
gpg: can't connect to the gpg-agent: IPC connect call failed
gpg: keydb_search failed: No agent running
gpg: skipped "<signing-key>": No agent running
gpg: signing failed: No agent running
error: gpg failed to sign the data
fatal: failed to write commit object
```
1) Start Agent
```bash
 gpgconf --launch gpg-agent
```
**To prepare for future use, set the Gpg agent to auto-start.**

- Search for "Task Scheduler" in the Windows Start menu.
- Click "Create Basic Task".
  -  Name the task "Gpg Agent Startup".
- Click next to move to "Trigger". Select "When I log on".
- Click next to move to "Action". Select "Start a program".
  - Under "Program/script" add:
  
  ``` bash
   gpgconf --launch gpg-agent
  ```
  Click next.
- Popup will appear asking:

    *"Do you want to run the following program: start "GPG Agent" "C:\Program Files (x86)\GnuPG\bin\gpg-connect-agent.exe" /bye."*

  Click Yes
- Click finish.
