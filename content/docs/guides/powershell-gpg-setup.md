---
Title: "Setting up GPG Keys with Powershell"
lastmod: 2024-02-12
draft: false
menu:
  docs:
    parent: "git"
weight: 10
toc: true

---

## Install GPGwin

  1. Download gpg4win:  https://gpg4win.org/  
  **Integrity Check** https://gpg4win.org/package-integrity.html

  2. Install gpg4win
  
  - You will see options to install several components:
      -  **GnuPG:** The core GnuPG engine for encryption, decryption, and key management. You must install this to generate GPG keys.
      - **Kleopatra:**  GUI for GnuPG. Recommended if you prefer to use the GUI.
      - **GpgOL:** A plugin for Outlook that allows you to encrypt, decrypt, sign, and verify emails using GPG. It is not needed for this project, but it is a good idea if you use Outlook..
      - **GpgEX:** Windows Explorer extension that context menu entries for encrypting, decrypting, signing, and verifying files using GPG.

## Generate GPG Key

1. Open PowerShell and run 

```bash
gpg --full-generate-key
```

- Choose the encryption type, which by default is ECC, which you can use to sign and encrypt.
- Choose how long you wish to have your key be valid for. 

{{< callout context="note" title="Note" icon="info-circle" >}} 
Although many people choose not to set an expiration date, I recommend against this practice as setting an expiration date on your keys is a good security practice. If you are worried that your keys will expire while they are still in use, remember that you can always extend the key's expiration date.
{{< /callout >}}

- Enter your information as prompted
- Choose a passphrase

## Confirm Key Creation
```bash
gpg --list-secret-keys --keyid-format LONG
```

*Note: The key ID is the part after / on the line starting with "sec"*
