---
title: "Docs"
#description: ""
#summary: ""
date: 2024-02-12
lastmod: 2024-02-12
draft: false
menu:
  docs:
    parent: ""
    identifier: "docs-home"
weight: 
toc: true
---
Learning new skills is always an adventure, and there's no need to journey alone. Within these pages, you'll find documentation I created for various projects I've delved into. See an error or have a suggestion? Please contribute! Merge requests are welcomed and will be reviewed.