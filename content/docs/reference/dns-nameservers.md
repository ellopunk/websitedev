---
Title: "DNS Root Servers"
lastmod: 2024-02-9
draft: false
menu:
  docs:
    parent: "Notes"
weight: 40
toc: true
---

|   Root Name Server    |    IP          |   Operator                            |   Software   | Country    |
|        ---            |    ---         |      ---                              |     ---      |   ---      |
|  a.root-servers.org   | 198.41.0.4     | Verisign                              | NSD and Verisign ATLAS | USA    |
|  b.root-servers.org   |  70.247.170.2  | University of Southern California     | BIND and KNOTT DNS    |  USA   |
|  c.root-servers.org   | 192.33.4.12    | Cogent Communications                 | BIND          | USA   |
|  d.root-servers.org   | 199.7.91.13    | University of Maryland                | NSD           | USA    |
|  e.root-servers.org   | 192.203.230.10 | NASA Ames Research                    |  BIND and NSD | USA    |
|  f.root-servers.org   | 192.5.5.241    | Internet Systems Consortium           | BIND          | USA   |
|  g*                   | 192.112.36.4   | Defense Information System Agency     | BIND          | USA  |
|  h.root-servers.org   | 198.97.190.53  | US Army Research Lab                  | BIND          | USA    |
|  i.root-servers.org   | 192.36.148.17  | NetNode Internet Exchange             | BIND          | Sweden    |
|  j.root-servers.org   | 192.58.128.30  | Verisign                              |  NSD and Verisign ATLAS|  USA   |
|  k.root-servers.org   | 193.0.14.129   | RIPE NCC                 | BIND, NSD, and KNOTT DNS    | Netherlands    |
|  l.root-servers.org   | 199.7.83.42    | ICANN                                 | NSDand Knott DNS    | USA     |
|  m.root-servers.org   | 202.12.27.33   | WIDE Project                          | BIND           | Japan    |

> The Defense Information System Agency unlike all other DNS root servers does not have a homepage under root-servers.org

> Netnode also distributes the official Swedish time through NTP
