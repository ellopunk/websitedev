---
Title: "Attack Types"
lastmod: 2024-2-18
draft: false
contributers: Ell Marquez
menu:
  docs:
    parent: "Notes"
weight: 20
toc: true
---
## Injection Attacks

Injection attacks exploit vulnerabilities in how user input is handled, allowing an attacker to inject and execute malicious code. This can lead to unauthorized access to data or compromise of a system.   
Injection attacks moved down from first place in 2017 to third in the [OWASP Top 10](https://owasp.org/www-project-top-ten/) 10 2021 release. 

### Types of Injection Attacks
  - **SQL Injection Attacks** occur when an attacker can insert malicious SQL statements in input fields. The input is executed by a backend database, allowing an attacker to access sensitive data, tamper or delete data, and may allow the attacker to have administrator access. 
  - **LDAP Injection Attacks** target applications that use the Lightweight Directory Access Protocol (LDAP) for authentication. Attackers can manipulate LDAP queries to perform unauthorized actions, such as accessing sensitive data or escalating privileges in the LDAP directory.
  - **Command Injection Attacks**  can exploit vulnerabilities in input handling to inject and allow an attacker to execute arbitrary commands, allowing unauthorized access to the OS and the ability to execute malicious scripts. 
  - **XPath Injection Attacks** target applications that use XML Path Language (XPath) queries to navigate XML documents. An attacker can manipulate XPath queries and bypass authentication to access sensitive data or modify the XML data. *(XPath is a query language used to query different parts of an XML document.)*
  - **[Cross-site Scripting Attacks]**(#cross-site-scripting-attacks)

### Injection Attack Prevention
  - Use an allow list that defines acceptable characters and formats for end-user input. 
  - Validate whether the data is in an expected format and sanitize user input by removing or escaping potentially harmful characters

## Cross-site Scripting Attacks
Cross-site scripting Attacks (XSS) use security vulnerabilities in web applications to inject malicious scripts, generally in the form of a browser-side script. *(A browser-side script, also known as a client-side script, refers to code executed on a web application's client side.)*

  - Using XSS attacks allows the attacker to send malicious scripts to the end user as the end-user has inherent trust in the website they visit.
  - Injected malicious scripts can access cookies and session tokens, allowing the attacker to impersonate the end user.
  - An attacker can display fake authentication portals, tricking the end user into imputing sensitive information.

### Types of Cross-site Scripting Attacks

  - **Reflected XSS**, also known as non-persistent or Type 1, is activated through a malicious link. This link is typically sent to an end user using social engineering tactics such as phishing emails. When the link is clicked, a request is sent to the website, enabling the execution of a malicious script. 
  - **Stored XSS**, also known as persistent XSS or type 2, occurs when a web application allows the end user to store data on the web application server. An attacker can use this ability to store malicious code on the server. When someone accesses the web application, the malicious code can execute in their browser.
  - **DOM-based XSS**, also known as Type-0 attacks, happens when an application's client-side script writes user input into the document object model (DOM) without proper sanitization, reads the data from the DOM, and executes the script. *(A document object model (DOM) is a programming API for HTML or XML documents that defines the logical structure of documents and the way these documents are accessed and manipulated.)*

### Cross-site Scripting Prevention
  - Use an allow list that defines acceptable characters and formats for end-user input. 
  - Validate whether the data is in an expected format and sanitize user input by removing or escaping potentially harmful characters. 
  - Use secure frameworks that come with built-in protections against XSS.


