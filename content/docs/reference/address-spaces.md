---
Title: "Understanding Memory Management in Computing Systems"
lastmod: 2024-02-9
draft: false
contributers: Ell Marquez
menu:
  docs:
    parent: "Notes"
weight: 10
toc: true
---

## Overview 

The primary goal of memory management is to provide a method for the OS or CPU to access memory efficiently while ensuring that each process operates independently without interfering with others.

### Types of Memory Addresses:

- **Physical Address Space:** Refers to the actual addresses in the RAM (Random Access Memory).

- **Virtual Address:** A layer of abstraction over the physical memory, allowing each process to have its own isolated memory.

### Important Components:
- **Memory Management Unit (MMU):** Hardware components that translate virtual addresses to physical addresses. This translation is vital for accessing the correct memory locations during process execution.

- **Page Tables:** Data structures the MMU uses to maintain the mapping between virtual and physical addresses, helping ensure that data retrieval and storage are conducted accurately and efficiently.

- **Process Isolation:** Each process is assigned a separate virtual address space providing a safeguard ensuring that one process cannot directly access the memory of another process 

### Memory Regions In Processes: 

- **Stack:** The memory region where parameters, local variables, and control data for the process are stored
- **Heap** - Used for dynamic memory allocation, the heap is managed by algorithms like 'malloc' in C programming, which allocate and free memory as needed during runtime.

### Shared Libraries and Mapped Files
 
 -Shared libraries alow multiple processes to use the same library or file without needing separate copies in memory.

- Memory-mapped files also enable I/O operations through memory manipulations, which can improve performance.

### Input/Output (I/O) Operations:

I/O operations transfer data to and from the CPU memory and peripheral device. 

- **Synchronous I/O:** The program waits or blocks until the I/O operation is complete.

- **Asynchronous I/O:** The program continues executing while the I/O operation is processed, which helps improve efficiency and responsiveness.

- **Direct Memory Access (DMA):** This technique allows data to be transferred directly between the memory and a device, bypassing the CPU to speed up operations significantly.

### Security in Memory Management

- **Address Space Layout Randomization (ASLR):** ASLR is a security technique that randomly arranges the position of key data areas within the process address space. 
    - ASLR can be bypassed if an attacker finds a memory leak vulnerability that reveals the address
    - ASLR does not protect against non-memory corruption bugs, such as logic errors or side-channel attacks.