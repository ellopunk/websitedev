---
Title: "Adding Code Blocks to Doks"
lastmod: 2024-02-14
draft: false
menu:
  docs:
    parent: "Notes"
weight: 60
toc: true
---
Writing documentation in markdown can be complicated when you want to add code blocks. Luckily, Doks developers have added formatting possibilities for code blocks in Doks 1.2; however, the documentation available for code blocks was complex for me to parse, so I simplified the information and decided to share it with you all. 

### Traditional Backtick Method
````md
```js
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
```
````
outputs: 

```js
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
```


### File Name Tab

````md
```js {title="count.js"}
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
```
````
Output: 

```js {title="count.js"}
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
```

### Terminal Window

````md
```bash
print("Sum of", num1, "and", num2 , "is", sum)
```
````
Output: 

```bash
print("Sum of", num1, "and", num2 , "is", sum)
```

#### With a Title

````md
```bash {title="adding numbers…"}
print("Sum of", num1, "and", num2 , "is", sum)
```
````
Output:

```bash {title="adding numbers…"}
print("Sum of", num1, "and", num2 , "is", sum)
```

### Line Numbers

````md
```js {lineNos=true lineNoStart=1}
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
```
````

```js {lineNos=true lineNoStart=1}
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
```

### Highlight

````md
```js {hl_lines=4}
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
```
````

```js {hl_lines=4}
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
```


#### Highlight With Line Numbers:


````md
```go {linenos=true,hl_lines=[1,"2-4"],linenostart=1}
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
```
````


```go {linenos=true,hl_lines=[1,"5-6"],linenostart=1}
# Adding two numbers
sum = num1 + num2
 
# printing values
print("Sum of", num1, "and", num2 , "is", sum)
print("I needed one more line for my example.")
```
