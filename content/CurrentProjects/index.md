---
#title: "Current"
description: "2024 Projects"
date: 2024-02-012
lastmod: 2024-02-013
draft: false
weight: 30
---

# 2024 Conference Presentations

## Once Upon a Cyber Threat: The Brothers Grimms Teachings on APT Awareness

**Abstract:**
Two hundred years ago, the first volume of fairy tales was published by the Brothers Grimm, introducing to the world a realm of magic, dark forests, and powerful villains to haunt everyone's dreams.  

We never imagined this realm would exist in the digital age. "Once Upon A Cyber Threat"  delves into the realm of advanced persistent Threat Groups (APTs), drawing parallels between the world of poisoned apples, breadcrumb trails, and magic mirrors and today's modern cyber threats. Serving not a tale of caution but a call to action and a lesson in storytelling, creating an outline that can help every security professional impart the caution, wisdom, and resilience we need to become the narrators that transformed Brother Grimm's tales into the happy ever after stories we know today. 

**Description:**

**Understanding Key Players and Motivations**
Like the characters in Grimm's fairy tales, every actor we face has their own motivation. Together, we will delve into the psyche of these villains to better understand how we can anticipate and build defenses before they act. 

**Current Solutions:** 
Looking to advance our tactics to match the perceived advanced nature of our attacker has led to an over-reliance on complex and sophisticated technologies, ignoring the breadcrumb trail right in front of us. Through this talk, we will discuss how our reliance on technology has weakened our security posture, leaving us vulnerable to the threats we seek to evade. 

**Returning to the Basics:**

 They say hindsight is 2020, yet we ignore how every adaptation of Grimm's fairytales is just that, an adaptation of a known story. In our race to adopt the latest security technologies, we need to research, educate, and plan our defenses through tactics like threat modeling so when we encounter the woodsman we know will be in the story, we are prepared for whether he turns out to be a friend or a foe. 

<hr/>

## Cloudy with a Chance of IAM Mistakes: Navigating the Skies of AWS Security

**Abstract:**
In the expanse of the cloud computing atmosphere, AWS dominates with an ever-growing offering of services, tools, and managed configurations. But like any weather system, it's not always clear skies. IAM (Identity and Access Management) stands as a lighthouse, guiding users safely through the storm of an ever-growing number of attacks and security breaches. Explore common IAM mistakes, study real-world examples of IAM misconfiguration that lead to breached cloud environments, and upgrade your cloud security knowledge to ensure your cloud isn't left hanging in the wind.


**Description:**
This talk focuses on the crucial role of IAM (Identity and Access Management) to help ensure a smooth sailing experience.

 We'll start by understanding why IAM is our lighthouse, exploring real "I-can't-believe-they-did-that" moments of actual cloud breaches caused by common IAM missteps. 

Are you worried about making IAM mistakes? Don't worry. This presentation will arm you with a map of best practices and guidelines to securely cross the AWS skies. By the end of our time together, you will be well-prepared to harness the power of IAM effectively, ensuring your AWS journey remains storm-free.

**What's the problem?**
- Unauthorized access leading to data breaches 
- Lack of training to adequately write policies capable of accommodating the dynamic nature of a constantly changing cloud environment
- The evolving strategies and malware used by attackers 
- Properly managing roles and users in a dynamic environment  

**What's the goal?**
- Build on our foundational IA  knowledge by exploring IAM components and highlighting IAM security risks.
- Delve into AWS-managed and customer-managed policies and go against the trend that customer-managed is always better. 
- Demonstrating best practices in developing and implementing IAM
- Gain knowledge of the tools available to audit and configure IAM policies

**How do we do it?** 
- Creating policies that address our ultimate goals but focus not on the big picture but on the foundations of a policy first
- Use previous breaches to create a blueprint for our security configurations
- View IAM not as a service but as a tool 
- Make use of but not solely rely on cloud-native IAM tools

**Key Takeaways:** 
Adapting AWS-managed policies to specific organizational use cases while maintaining security and compliance.
Understanding the significance of IAM in cloud security
Knowing common IAM mistakes and their real-world consequences 
Creating a blueprint for building and adapting IAM policies 
Familiarization of tools and resources available 

<hr/>

## From Food Fights to Feast: Cooking Up Trust Between Developers and Security Teams


**Abstract:** Navigating the software process can sometimes feel like two cooks in a kitchen—developers sprinkling some code spice and security teams taste-testing every bite. And sure, there might be some mild food fights along the way. In this talk, we're swapping our security hats for empathy goggles, diving deep into the world of developers. Join me as we decode what we, the mighty gatekeepers of safety, can do better. Let's whip up the perfect recipe for trust, ensuring developers and security pros are the PB&J of the software sandwich!

Picture this: one big, happy tech fam producing software as perfect as a five-star dinner. Tasty, top-notch, and no nasty surprises when you dig in!

**Description:**  Why am I the right chef to whip up this delicious dish of a talk? Aside from my decade-long gourmet experience in system administration, instructing, and championing security, I've played an unofficial therapist for our brooding developers and the ever-watchful security personnel.

What's the problem: 
Lack of communication
Lack of training
A culture of blame
Conflicting priorities 
Unrealistic expectations. 

**What's the goal?:**  This presentation will not focus on the traditional conversation about how we can all shift left together. Instead, we will have an open and brutally honest discussion about the current state of DevSecOps. We will bust some myths and face the consequences of our contemptuous history to build actionable strategies to create secure code and stronger teams. 

**How do we do it?** 
***Communication:*** Ensuring that everyone, from developers to security teams and beyond, shares a common understanding and approach to security practices. 

***Collaboration:*** Moving away from focusing on embracing terminologies such as DevOps and DevSec ops and concentrating instead on upholding the principles behind them. 

***Catalyzation:*** Adopting a security-first approach, security should be considered from the early stages of the development process, and security measures should be built into the software from the ground up.

**Key takeaways:**
Attendees will walk away with actionable strategies and tools to aid them in establishing channels for open communication and frequent synchronization to help nurture a sense of understanding and trust between these two crucial software development components. Additionally, how to build a culture that acknowledges and celebrates security wins, providing due recognition to developers for their diligent efforts in adhering to secure coding practices. This multifaceted approach envisions a future where cooperation flourishes, resulting in more secure and resilient software products.
