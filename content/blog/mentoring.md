---
title: "The Power of a Moment: Making a Difference Through Mentoring"
description: ""
summary: ""
date: 2018-08-17
lastmod: 2024-02-16
draft: false
weight: 500
categories: [Community]
tags: [Mentoring, Openstack]
contributors: [Ell Marquez]
pinned: false
homepage: false
---
I’ve been one of the lucky few who has never known a time without a mentor.

I entered the tech community through the Open Cloud Academy, a program in San Antonio run by Rackspace that teaches system administration to anyone who wants to learn regardless of technical background. During my time at the OCA, a handful of volunteers came by to help us learn how to troubleshoot and teach us what it means to be a sys admin. I didn’t know it at the time, but they were my first mentors.

Merriam-Webster defines a mentor as a trusted counselor or guide, but if I’m going to be honest (and I always try to be), for anyone who has had a mentor, the word means so much more. However, I also believe that we’ve been looking at mentorship in the wrong light for far too long. We view mentorships with these formal dynamics and put as much pressure on them to succeed as we do on business deals. That’s why so many mentorships never go beyond the planning stages. I once heard a saying that people come into your life for a reason, a season or a lifetime and that’s the perspective we need to adopt for mentorship.

When Rackers came by the OCA to help, they did so for a reason; they were there to offer guidance to what everyone hoped would be the next crop of Rackspace rookies. There was no formal mentorship program, no official ask of these employees. Some came because they have a volunteer’s heart. Others because they had once been students at the OCA and wanted to repay the favor.

As time passed, it became evident which pairing of students and volunteers worked best. We began to see groups working closer together and exchanging information so they could continue to collaborate outside the OCA. Though some would argue that’s not mentorship, I would argue that it’s time to expand the definition of mentor.

I believe a mentor is someone who can not only help you recognize your strengths, but also help you see the areas where you need to develop to achieve your goals. It’s challenging to create this type of relationship instantly, and because of this, we need to be open to allowing that dynamic to grow organically. Sometimes the first steps to a long-term mentorship can be a few moments where someone gives their time to help foster the growth of someone else. When this happens, the next step requires a moment of vulnerability from the mentee. They have to admit that they could benefit from additional help and then take the risk of asking for it. It can be as simple as saying “I learned so much in our time together. Would you mind if I followed up with you if I have more questions?” This simple comment can help establish the reason.

> We view mentorships with these formal dynamics and put as much pressure on them to succeed as we do on business deals. That’s why so many never go beyond the planning stages.

This moment of vulnerability lets the mentor know that their time was well spent and that their effort has value. However, as mentees, our jobs are not done. We need to make an effort to continue our journey on our own, to put what we have learned to work.

I recently had a chance to test my own advice when I decided to learn to program by writing a small Python script to keep track of my grocery list. When I ran the idea by a colleague, I could instantly see the look of panic, accompanied by the comment “I wish I could help but I already have so much on my plate.” I’ve seen this look before; he thought I was asking to take on the responsibility of teaching me. So a few weeks later, when I ran into a few issues where the code didn’t behave how I thought it should, I took the time to prepare my questions before approaching him for help.

Instead of saying, “Can you help me with my code?” or “Ugh, this is broken, and I don’t know why. Could you help?” I narrowed down the area in which the issue was occurring and asked, “The logic in this if-then statement does not seem to be occurring the way I thought it would. If I send it to you, could you look it over?”   The phrasing of the question allowed him to understand that I wasn’t asking for a significant amount of time debugging my entire program or even asking to teach me how to code. I was merely asking for a few minutes of troubleshooting. The result was better than expected. My colleague was enthusiastic about how much I’d managed to accomplish on my own and he explained how my indentations caused a break in the logic of the code. He also offered a few other suggestions on how to improve the script.

A week later, after running into another issue implementing one of his recommendations, I asked if he’d look again. Because I’d previously shown that I was not only willing to put the work in myself but also valued input, his willingness to help was genuine. The mentorship relationship had been seeded without any formal commitment. In the two months since I decided to learn to program, this colleague has found a renewed interest in strengthening his own Python skills along with becoming one of my most prominent advocates and mentors.

The point of sharing my story is to challenge you to not look at mentorship as a formal agreement between two people, but to look for it in the small moments where someone offers to help. Be willing to open up and perhaps feel a moment of vulnerability by admitting how much you could benefit from continued support. The OpenStack and Open source community is full of talented, caring individuals who want to help the community grow; we have to be willing to invest the time to help these relationships evolve.