---
title: "Old Blog Post"
description: "Here you will find blog posts from previous years."
summary: ""
draft: false
weight: 50
categories: []
tags: []
contributors: []
---
Contributions to community programs through blog posts, as well as articles written for past employers.