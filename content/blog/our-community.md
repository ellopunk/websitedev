---
title: "Our Community"
description: ""
summary: ""
date: 2018-08-14
lastmod: 2024-02-16
draft: false
weight: 550
categories: [Community]
tags: [Mentoring]
contributors: [Ell Marquez]
pinned: false
homepage: false
---
*This blog was originally written for and published by: Linux Academy*

Hello again,

I did not plan on writing again so soon, and although I try my hardest to be a positive person, I have been facing some of my demons as of late. I thought that others might be in the same place as myself and that my thoughts may be able to help them or maybe some of you are willing to share your thoughts with me. In the last few weeks, I have made an enormous transition by leaving the company that had been my home for four years to join Linux Academy. Although it was a decision that I made quickly, it was not a decision that I made lightly.

I met Anthony when I was volunteering for Texas LinuxFest, and after a short conversation he looked right at me and said, “Why don’t you work for Linux Academy?” I did manage to bite back my instinctive reaction of, “I didn’t know you all would even consider me,” and simply said, “I don’t know.”


When I got home, I did a bit of research on the company, and I found Anthony’s article about Linux Academy’s mission values; they spoke to me in a way that I did not expect. I talk about this in more detail in my previous article [“It’s Okay to be New.”](/blog/its-okay-to-be-new) No one should ever feel that they are too new, do not know enough, or will never get to the point where they feel comfortable working with any technology. However, if I am being honest with you, it is not always so easy.

When I worked in Linux Support, I worked with enterprise companies. If I made a mistake, it could easily become extremely costly. At one point in my career, I accidentally restarted all of the nodes of a MySQL cluster at the same time. I’m sure plenty of you are laughing right now at how ridiculous that sounds but I was new, and I did not understand how clustering worked. This is the dilemma, how can you be new and feel comfortable making mistakes if your mistakes can have costly consequences? After this experience, I became trigger shy and became the tech who always targeted the low hanging fruit; queen of the low disk space tickets. I was lucky enough to have a mentor and a team who helped pull me out of that mindset and gave me the safety net I needed to try again.

As I progressed in my career and began to speak at conferences on Imposter Syndrom and the struggles of being new to the industry, I learned that I was not alone, though not everyone I met was lucky enough to have that safety net set for them.

I watched amazing people leave the industry because they became burned out or convinced they could not be “good enough.” So when I read Linux Academy’s core values, I stared for a very long time at one in particular: 

*“We believe in the curiosity to learn, the vulnerability to try, the persistence to succeed, and the strength of community."* 

**This!** This is what was missing in our community. A safe place, somewhere that we can let down our guard, a place where we can admit that we don’t know and ask questions; somewhere we can restart all the nodes in a MySQL cluster without causing a customer downtime.

Now some of you may be thinking, *“Great she’s trying to sell me on joining Linux Academy,”* but I promise you that you are wrong. I want to sell you on the message. I want the Linux and OpenSource communities to continue to grow and change in such a way that we are not losing people because they become afraid to grow because their last mistake was so costly or because they are too scared to ask for help because everyone will see how much they don’t know. Its been over a year since I have worked as a Linux Administrator and I still struggle with hitting these walls. I was so used to looking at a ticket queue and ticket stats to see my value as a tech that not having that metric to measure myself anymore scared me. So I turned to my community outside of Linux Academy and showed vulnerability by expressing my fear, and you know something? I’m glad I did because not only did people offer support but they offered help. One of these people, entirely to my surprise, was Anthony.

After talking to him about the walls I was hitting, he reminded me that Linux Academy has more than just one mission value:

 *“We support others in their mission to learn and grow. We are committed to changing the world by changing lives.”*

If I was living those values; if I was working every day to help others grow, to help change lives, I did not need to worry so much about those numbers. In the end, I think I wrote this article to say if you are hitting those walls and you are working without a safety net, please do not feel that you are alone. You do not have to have a Linux Academy account to join us on our slack community, and you do not have to have a paid account to be a part of our community forum because there is one more mission statement that leads our work: *“We put our students’ futures first.”* Sometimes that means focusing on more than just pushing the boundaries of online learning; sometimes it means pushing the boundaries to help change our community.

**Building a community one student at a time!**