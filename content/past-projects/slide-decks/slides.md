---
title: "Resources for Previous Projects"
#description: ""
#summary: ""
date: 2024-02-12
lastmod: 2024-02-12
draft: false
menu:
  docs:
    parent: ""
    identifier: "Past-proj-slides-list"
weight: 100
toc: true
---

## Essential Container Concepts 
This course provided a basic introduction to the concept of containers. Seeking to give you a working knowledge of what containers are, how they are created, and how they are used in the real world.

[Course Resources](https://github.com/Ellopunk/content-container-essentials-101presentation?to=/placeholder.com)

[Pinehead Press - Essential Container Concepts by - Ell Marquez](https://lucid.app/documents/embeddedchart/681cbd68-2dfb-4b57-b022-6378490d31d3#)

[Shared Resource by Student Rubaiyat Rahim](https://medium.com/@rubaiyat.rahim/docker-basics-5affdcc9289a)

## Container 101 - Getting Up and Running with Docker Containers
Being new to containers can feel overwhelming. This workshop dives into what containers and images actually are, best practices around them, how to use volumes and port mappings, and how to run multi-service applications. Hands-on opportunities and training from Docker Captains are provided, but orchestration and running containers in production are not covered.

[Slides](https://speakerdeck.com/mikesir87/container-101-getting-up-and-running-with-docker-containers)

## Goal Setting 101
Abstract: This short talk focuses on how to turn goals and dreams into practical SMART goals. It shows how using SMART goals can provide a foundation to approach mentors, managers, or face impostor syndrome head-on.

[Slides](https://slides.com/jupiterbroadcasting/mentoring-101-7)


## Help Request

Through the years, presentation materials have been lost in job transitions. Please get in touch with me or create a merge request if you have access to any materials or recordings from these presentations. 

#### Containers: What you need to know; so you know what you need to know
The number one question that someone starting out with container technology always seems to have is "Where do I get started?" This talk goes over the basics of container technology by answering the basic questions: who, what, when, where, and why, to help those who feel lost or don't know enough to even know what questions to ask.

#### Creating a Stronger Community by Poisoning Your Own Well
Abstract: This talk discusses the shift in mindset within the rapidly growing technology industry towards diversification and community development. It addresses the issue of Imposter syndrome and suggests that the solution should not be placed on the individual alone, highlighting a toxic culture that undermines those it intends to build up.

#### I’m going to be an OpenStack Master!: A Hands-On Workshop for preparing for the COA
This workshop is designed for OpenStack users to overview the skills needed to pass the OpenStack Certified Administrator Exam, offering experience in both the Horizon Dashboard and the CLI.

#### OpenStack: A Day in the Life of an Admin
This workshop offers a walk-through of tasks an OpenStack administrator might perform using the Horizon interface, tailored for those newly hired as Linux administrators to understand their role better. It highlights Rackspace Training Summit Workshops' approach to making learning OpenStack accessible and effective.
