---
title: "Presentation Slide Decks"
description: "Resources from presentations with no recordings available.."
summary: ""
date: 2024-02-22
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: ""
    identifier: "Past-Projects-slides"
weight: 400
toc: true
---