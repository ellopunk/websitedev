---
title: "Side Projects"
description: "Presentation videos that didn't fit elsewhere."
summary: ""
date: 2024-02-22
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: ""
    identifier: "Past-Projects-side"
weight: 100
toc: true
---

## Ell's Cyber Security Notable Malware 2021
There were so many new variants of malware detected in 2021, yet one little 
🦆 remains my favorite
[Watch](https://youtu.be/a7B8dYcdedQ)

## Linux This Month 
[Introducing Linux This Month](https://youtu.be/evSYmhtchlE?feature=shared)

## Forensic Happy Hour Episode 207
[Watch](https://www.youtube.com/live/kh4EuktE_Ok)

## Cloud Engineering Summit 2021: Manage Panel

Join Ell Marquez, Niall Murphy, Jeff Smith, and Sasha Rosenbaum as they discuss topics related to Cloud Engineering.
[Watch Panel Discussion](https://youtu.be/vzb70EvK51o)

## Is Linux Secure by Default? 
 
The Linux operating system is secure by default; that is because Windows is the most used operating system in the world and thus attackers create viruses and malware to target Windows systems. While cringeworthy to read, these are all real statements made to me when I started studying Linux administration. This is a false narrative that is still frequently touted in the technological world. 
 
Linux has become the predominant operating system for IoT devices, web servers, and cloud servers. Attackers are porting their existing Windows tools to Linux and they are developing an extensive collection of unique Linux malware tools. A recently discovered example is IPStorm, a botnet previously documented as only targeting Windows, now targeting the Linux platform.


How do we detect new threats? The most effective way to detect most threats is by monitoring code in runtime, since executing unauthorized or malicious code is something that all cyber attacks have in common.

[Watch](https://youtu.be/h8cQ_hAdC_c)
