---
title: "BrightTalk Webinars"
description: ""
summary: ""
date: 2024-02-22
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: ""
    identifier: "Past-Projects-Brighttalk"
weight: 300
toc: true

---
**Note:** A BrightTalk account will be needed as this is gated content.

## Leveraging the Assume Breach Mentality

**Presented by:**
Ell Marquez, Linux Security Advocate, Intezer Labs; Brandon Dunlap, Moderator


With a staggering growth in the number of vulnerabilities and a constantly changing attack surface, companies begin their transition to the cloud at a disadvantage in their security posture. Threat actors understand this and better than we do and are quickly morphing their traditional attacks to take advantage of the situation. At the end of the day there was fact that all companies must accept, breaches happen. On November 2, 2021 at 1:00pm BST join Ell Marquez, Linux Security Advocate at Intezer, for exploring how assume breach doesn’t mean all the money you spent on security solutions is going to waste and what really is meant by assume breach and what makes an effective assume breach strategy.

[Watch](https://www.brighttalk.com/webcast/13279/513493)

## A Muggles Guide to Security In The Cloud

In the security and technology world, we rely so heavily on buzz words to explain our work that others feel like we are magicians working spells that they will never be able to do.

Saying, "Due to issues with our security posture, the APT manipulated a well-known CVE to breach our cloud-native-applications." Might as well be: "The Death Eaters were able to use a port key to enter our environment and effectively cast the Avada Kedavra spell."

Instead, we could say, "An attacker used a known flaw to gain access to our environment and brought down our servers."

In this session, we will come to understand that security for our cloud environments can be simple to understand, yes even for muggles. That is, if we focus on the root cause of all cyber attacks: unauthorized spells, wait, I mean unauthorized code.

[Watch](https://www.brighttalk.com/webcast/17475/457270)

## A Wizards Guide to Security in the Cloud

A Horcrux is a powerful object in which a Dark wizard or witch [attacker] has hidden a fragment of his or her soul [code] for the purpose of attaining immortality [persistence].

Creating a Horcrux gives one the ability to anchor their own soul [code] to earth[environment], if the body [process] is destroyed.

In this session, we will come to understand how attackers are able to not only compromise our cloud environments but also maintain persistence—while our security teams are distracted by a mountain of false alerts. If we focus on the root cause of all cyber attacks: unauthorized spells, wait, I mean unauthorized code.

[Watch](https://www.brighttalk.com/webcast/17475/458646)

## Without a Trace: The Dangers of Fileless Malware in the Cloud

**Presented by:**
Ell Marquez, Linux and Security Advocate, Intezer Labs; Brandon Dunlap, Moderator

About this talk
Every day, wars are being waged on invisible battlefields. The enemy is hiding and stealthily leveling its attacks from within. This formidable foe isn’t an opposing army. It may very well be a single malicious actor, or a state-sponsored group of hackers. Without a trace of their tools left on the disk, attackers are storing the code in memory–resulting in infamous Fileless Malware. If successful, the best case scenario outcome is a tarnished reputation; the worst, significant (and potentially irreparable) damage to a brand and its business. Join Intezer Labs and (ISC)2 on May 25, 2021 at 1:00 pm BST for a discussion on how attacks like these can cripple an organization without its security team ever knowing it.

[Watch](https://www.brighttalk.com/webcast/17475/482245)



