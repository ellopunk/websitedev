---
title: "Amazing SANS Webcast"
description: ""
summary: ""
date: 2024-02-22
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: ""
    identifier: "Past-Projects-SANS"
weight: 100
toc: true
---

## First Contact with Container Security - Resistance is Futile

In the cloud companies are transitioning to the use of microservices at a rapid pace. While this model decreases time to market, it also increases supply chain security risk and lowers visibility. According to the Cloud Native Computing Foundation, 92% of companies surveyed are using containers in their production environments. It seems that when it comes to transitioning part of your cloud ecosystem, resistance is futile.

Your containers are likely hosting applications that deliver content to customers. Meaning that your container runtime is exposed to the internet. As modern runtime environments are complex, they present multiple attack vectors. Even the best security is not a guarantee against an attack 'making comprehensive and active runtime protection all the more essential.

Intezer's Ell Marquez and SANS Institute's Jake Williams will explain how increased visibility into each deck (containers) environment (code) 'can help you act quickly to prevent your environment from being assimilated by attackers.

[Watch SANS Webcast](https://www.sans.org/webcasts/first-contact-container-security-resistance-futile-118415/)


## Challenges Incident Response Teams Face and How to Solve Them

A suspicious activity occurs on an endpoint so the incident response team launches an investigation to find out what happened. Did a breach occur? Are there any other infected endpoints? What was the purpose of the attack? How was the endpoint compromised?

The challenge IR teams face is to classify the collected data and find the malicious files, in the shortest period of time. Time is a major factor to reduce the potential damage of the environment. To accomplish this task the IR team will collect forensic evidence from one or more of the following sources: HD forensics, memory forensics, and live forensics. At this point, they will have hundreds, if not thousands, of files and information that need to be analyzed and classified. This analysis is time-consuming and should be done carefully to not miss any artifacts crucial to the investigation. It can often be like looking for a needle in the haystack.

Fortunately, there are ways to accelerate the forensics methods mentioned above. Learn how you can quickly classify and analyze the mass of files and memory dumps collected using these forensics methods in less time than ever.

Plus, learn how to integrate with the handy Volatility plugin, to get the comprehensive and automatic analysis of the loaded process along with the powerful capabilities of the plugin.

[Watch SANS Webcast](https://www.sans.org/webcasts/challenges-incident-response-teams-face-and-how-to-solve-them/)
