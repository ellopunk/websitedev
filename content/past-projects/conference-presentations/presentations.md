---
title: "Conference Presentation Videos"
description: ""
summary: ""
date: 2024-02-22
lastmod: 2024-02-22
draft: false
menu:
  docs:
    parent: ""
    identifier: "Past-Projects-SANS"
weight: 100
toc: true

---

## Confessions of a Sysadmin
This talk shares a practical approach to learning to navigate the command line and secrets learned along the way. It touches on awkward moments experienced in the tech community, from mansplaining to being told that real admins only use Arch, in hopes of inspiring the next generation. 

[Watch SCaLE 18x Presentation](https://youtu.be/1SFt0R1iWZA?feature=shared)

## A Muggle's Guide To Security In The Cloud
In the security and technology world, we rely so heavily on buzz words to explain our work that others feel like we are magicians working spells that they will never be able to do.

Saying: “Due to issues with our security posture, the APT manipulated a well-known CVE to breach our cloud-native-applications.” might as well be: “The Death Eaters were able to use a portkey to enter our environment and effectively cast the Avada Kedavra spell."

Instead, we could say, “An attacker used a known flaw to gain access to our environment and brought down our servers."

[Watch BSidesCLT Presentation](https://youtu.be/diBDcIUBVTg?feature=shared)

## The Missing Piece of Cloud Security
Migration to the cloud brings new challenges, from new vulnerabilities to a constantly changing attack surface. These challenges have not gone unnoticed by cyber criminals. Cloud based attacks have been so widespread that the Department of Homeland Security has warned of an increase of nation states, criminal groups and hacktivists against cloud based enterprise resources. Attackers are rapidly modifying their current tools to this new attack surface. The solution? In order to detect and respond to a breach, companies need visibility into what code is actively running on their systems.

[Watch BSidesCLT Presentation](https://youtu.be/2xoZViF9KYQ?feature=shared)

[Watch HackConf Co-Presented Presentation](https://youtu.be/2sureO2dBjk?feature=shared)


## Containers: No, Not Your Mama's Tupperware
This talk explores the evolution of containerization technology, from virtualization and Linux containers to the basics of Docker and Kubernetes, clarifying the buzz around containers.

[Watch Bsides San Antonio Presentation](https://www.youtube.com/watch?v=ZHa_qrwdkns)

## First Contact with Container Security
In the cloud companies are transitioning to the use of microservices at a rapid pace. While this model decreases time to market, it also increases supply chain security risk and lowers visibility. According to the Cloud Native Computing Foundation, 92% of companies surveyed are using containers in their production environments. It seems that when it comes to transitioning part of your cloud ecosystem, resistance is futile.

Your containers are likely hosting applications that deliver content to customers. Meaning that your container runtime is exposed to the internet. As modern runtime environments are complex, they present multiple attack vectors. Even the best security is not a guarantee against an attack.

In this session the speaker will discuss how we can mount our last line of defense when the Enterprise is breached, keeping our crew (data) from being assimilated.

[Watch BSidesRDU Presentation](https://youtu.be/aL4chmkkW4U?si=qTL6xKwCPRLv2Wrp)


## Mentorship 101 
[Watch The Linux Foundation Presentation](https://youtu.be/ryM6Hae3l6w?feature=shared)

## The Fallacy of DevSecOps 
[Watch GRIMMCon 0x7 Presentation](https://youtu.be/1LwP604Gmz4?si=jlxKyyEN6xUAEBpl)

## A Cure For Your Purple Haze 
[SOCStock 2021(Gated Content)](https://www.socstock2021.com/)